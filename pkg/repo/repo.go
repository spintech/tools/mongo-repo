package repo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/spintech/tools/mongo-repo/pkg/db"
)

type Repo struct {
	*db.Client
	CollectionName string
}

func New(client *db.Client, collectionName string) *Repo {
	return &Repo{
		Client:         client,
		CollectionName: collectionName,
	}
}

func (r *Repo) Collection() *mongo.Collection {
	return r.Database(r.DatabaseName).
		Collection(r.CollectionName)
}

func (r *Repo) All(ctx context.Context, extractor func(cursor *mongo.Cursor)) error {
	return r.Find(ctx, bson.D{}, extractor)
}

func (r *Repo) Find(ctx context.Context, filter bson.D, extractor func(cursor *mongo.Cursor), opts ...*options.FindOptions) error {
	cur, err := r.Collection().Find(ctx, filter, opts...)
	if err != nil {
		return err
	}

	defer func() { _ = cur.Close(ctx) }()

	for cur.Next(ctx) {
		extractor(cur)
	}

	if err := cur.Err(); err != nil {
		return err
	}

	return nil
}

func (r *Repo) Count(ctx context.Context) (count int64, err error) {
	return r.CountBy(ctx, bson.D{})
}

func (r *Repo) CountBy(ctx context.Context, filter bson.D) (count int64, err error) {
	return r.Collection().CountDocuments(ctx, filter)
}

func (r *Repo) Add(ctx context.Context, item interface{}) (primitive.ObjectID, error) {
	insertResult, err := r.Collection().InsertOne(ctx, item)
	if err != nil {
		return primitive.NilObjectID, err
	}

	id := insertResult.InsertedID.(primitive.ObjectID)
	return id, nil
}

func (r *Repo) Get(ctx context.Context, id primitive.ObjectID, extractor func(result *mongo.SingleResult) error) error {
	filter := bson.D{{"_id", id}}
	return r.GetBy(ctx, filter, extractor)
}

func (r *Repo) GetBy(ctx context.Context, filter bson.D, extractor func(result *mongo.SingleResult) error) error {
	res := r.Collection().FindOne(ctx, filter)
	if err := extractor(res); err != nil {
		return err
	}
	return nil
}

func (r *Repo) Delete(ctx context.Context, id primitive.ObjectID) error {
	filter := bson.D{{"_id", id}}
	_, err := r.Collection().DeleteOne(ctx, filter)
	return err
}

func (r *Repo) Update(ctx context.Context, id primitive.ObjectID, update interface{}) error {
	filter := bson.D{{"_id", id}}
	_, err := r.Collection().UpdateOne(ctx, filter, bson.D{{"$set", update}})

	// TODO: add logging
	// fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)

	return err
}
