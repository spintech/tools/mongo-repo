package repo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BaseRepo interface {
	Count(ctx context.Context) (count int64, err error)
	CountBy(ctx context.Context, filter bson.D) (count int64, err error)
	Add(ctx context.Context, item interface{}) (primitive.ObjectID, error)
	Delete(ctx context.Context, id primitive.ObjectID) error
	Update(ctx context.Context, id primitive.ObjectID, update interface{}) error
}
