package db

import "go.mongodb.org/mongo-driver/mongo"

type Client struct {
	*mongo.Client
	DatabaseName string
}

func NewClient(connectionString string, databaseName string) *Client {
	return &Client{Client: Connect(connectionString), DatabaseName: databaseName}
}
