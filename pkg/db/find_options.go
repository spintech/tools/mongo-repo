package db

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Paginate(opts *options.FindOptions, page, perPage int) *options.FindOptions {
	p, pp := int64(page), int64(perPage)
	return opts.SetSkip(p * pp).SetLimit(pp)
}

func SortAsc(opts *options.FindOptions, field string) *options.FindOptions {
	return opts.SetSort(bson.D{{sortField(field), 1}})
}

func SortDesc(opts *options.FindOptions, field string) *options.FindOptions {
	return opts.SetSort(bson.D{{sortField(field), -1}})
}

func sortField(field string) (f string) {
	switch field {
	case "created_at", "id":
		f = "_id"
	default:
		f = field
	}
	return
}
