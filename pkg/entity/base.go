package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Entity struct {
	Id        primitive.ObjectID `bson:"_id"`
	UpdatedAt primitive.DateTime `bson:"updated_at"`
}

func (m Entity) CreatedAt() time.Time {
	return m.Id.Timestamp()
}

func (m Entity) SetUpdatedAt() {
	m.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())
}

func New() Entity {
	return Entity{
		Id:        primitive.NewObjectID(),
		UpdatedAt: primitive.NewDateTimeFromTime(time.Now()),
	}
}

