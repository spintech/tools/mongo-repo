module gitlab.com/spintech/tools/mongo-repo

go 1.14

require go.mongodb.org/mongo-driver v1.3.4
